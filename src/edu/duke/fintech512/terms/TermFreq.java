package edu.duke.fintech512.terms;

import java.io.*;
import java.util.*;

/**
 * @author Minglun Zhang
 *
 */
public class TermFreq {

    // self-defined object
    public static class MyObj implements Comparable<MyObj> {
        private final String  word;
        private final Integer count;

        public MyObj ( final String word, final Integer count ) {
            this.word = word;
            this.count = count;
        }

        public String getWord () {
            return word;
        }

        public Integer getCount () {
            return count;
        }

        @Override
        public int compareTo ( final MyObj m ) {
            if ( getCount().compareTo( m.getCount() ) == 0 ) {
                return 0 - getWord().compareTo( m.getWord() );
            }
            else {
                return getCount().compareTo( m.getCount() );
            }
        }
    }

    public static String run ( String in ) {
        String rtn = "";
        // stop words cite: https://gist.github.com/sebleier/554280
        final String[] stopwords = { "i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your",
                "yours", "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it",
                "its", "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom",
                "this", "that", "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have",
                "has", "had", "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or",
                "because", "as", "until", "while", "of", "at", "by", "for", "with", "about", "against", "between",
                "into", "through", "during", "before", "after", "above", "below", "to", "from", "up", "down", "in",
                "out", "on", "off", "over", "under", "again", "further", "then", "once", "here", "there", "when",
                "where", "why", "how", "all", "any", "both", "each", "few", "more", "most", "other", "some", "such",
                "no", "nor", "not", "only", "own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just",
                "don", "should", "now" };
        final Hashtable<String, Integer> dic = new Hashtable<String, Integer>();
        try {
            final File input = new File( in );
            @SuppressWarnings ( "resource" )
            final Scanner reader = new Scanner( input );
            while ( reader.hasNext() ) {
                final String word = reader.next().replaceAll( "[^a-zA-Z]", "" ).toLowerCase();
                if ( dic.containsKey( word ) ) {
                    final int count = dic.get( word );
                    dic.replace( word, count + 1 );
                }
                else {
                    dic.put( word, 1 );
                }
            }
        }
        catch ( final FileNotFoundException e ) {
            throw new IllegalArgumentException( "File not Found" );
        }
        // remove stop words
        for ( int i = 0; i < stopwords.length; i++ ) {
            dic.remove( stopwords[i] );
        }
        final ArrayList<MyObj> printable = new ArrayList<MyObj>();
        dic.forEach( ( k, v ) -> {
            final MyObj obj = new MyObj( k, v );

            printable.add( obj );

        } );
        Collections.sort( printable );
        Collections.reverse( printable );
        rtn += "Output:\n";
        int size = 25;
        if ( printable.size() < size ) {
            size = printable.size();
        }
        for ( int i = 0; i < size; i++ ) {
            rtn += printable.get( i ).getWord() + " - ";
            rtn += printable.get( i ).getCount() + "\n";
        }
        return rtn;
    }

    /**
     * The main function
     *
     * @param args
     */
    public static void main ( String[] args ) {
        if ( args.length != 1 ) {
            throw new IllegalArgumentException( "One input file need." );
        }
        String print = run( args[0] );
        System.out.println( print );
    }
}
