package edu.duke.fintech512.terms;

import static org.junit.Assert.*;

import org.junit.jupiter.api.*;

class KWICTest {

    @Test
    void test () {
        String input = "is\n" + "the\n" + "of\n" + "and\n" + "as\n" + "a\n" + "but\n" + "::\n" + "Descent of Man\n"
                + "The Ascent of Man\n" + "The Old Man and The Sea\n" + "A Portrait of The Artist As a Young Man\n"
                + "A Man is a Man but Bubblesort IS A DOG\n";
        String input1 = "is!\r\n" + "the\r\n" + "of\r\n" + "and\r\n" + "as\r\n" + "a\r\n" + "but\r\n" + "::\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n";
        String input2 = "iswerwerwer\r\n" + "the\r\n" + "of\r\n" + "and\r\n" + "as\r\n" + "a\r\n" + "but\r\n" + "::\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n" + "";
        String input3 = "is\r\n" + "the\r\n" + "of\r\n" + "and\r\n" + "as\r\n" + "a\r\n" + "but\r\n" + "::\r\n"
                + "Descent of Man man man man man man man man man man man man man man man man man\r\n"
                + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n" + "";
        String input4 = "is\r\n" + "the\r\n" + "of\r\n" + "and\r\n" + "as\r\n" + "a\r\n" + "but\r\n" + "ask\r\n"
                + "petty\r\n" + "unique\r\n" + "mold\r\n" + "access\r\n" + "tolerant\r\n" + "fitness\r\n" + "ignite\r\n"
                + "begin\r\n" + "breathe\r\n" + "sip\r\n" + "business\r\n" + "essay\r\n" + "cabinet\r\n"
                + "majority\r\n" + "like\r\n" + "software\r\n" + "revise\r\n" + "literacy\r\n" + "award\r\n"
                + "truck\r\n" + "coincide\r\n" + "slogan\r\n" + "open\r\n" + "construct\r\n" + "center\r\n"
                + "retired\r\n" + "robot\r\n" + "excavate\r\n" + "absence\r\n" + "shorts\r\n" + "lost\r\n"
                + "slippery\r\n" + "line\r\n" + "inflate\r\n" + "deport\r\n" + "contract\r\n" + "flag\r\n" + "list\r\n"
                + "family\r\n" + "bat\r\n" + "sweat\r\n" + "crash\r\n" + "dinner\r\n" + "feather\r\n" + "core\r\n"
                + "absorb\r\n" + "if\r\n" + "pin\r\n" + "frighten\r\n" + "::\r\n" + "Descent of Man\r\n"
                + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n" + "";
        String input5 = "is\r\n" + "the\r\n" + "of\r\n" + "and\r\n" + "as\r\n" + "a\r\n" + "but\r\n" + "::\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n"
                + "Descent of Man\r\n" + "The Ascent of Man\r\n" + "The Old Man and The Sea\r\n"
                + "A Portrait of The Artist As a Young Man\r\n" + "A Man is a Man but Bubblesort IS A DOG\r\n" + "\r\n"
                + "";
        String output = "a portrait of the ARTIST as a young man\n" + "the ASCENT of man\n"
                + "a man is a man but BUBBLESORT is a dog\n" + "DESCENT of man\n"
                + "a man is a man but bubblesort is a DOG\n" + "descent of MAN\n" + "the ascent of MAN\n"
                + "the old MAN and the sea\n" + "a portrait of the artist as a young MAN\n"
                + "a MAN is a man but bubblesort is a dog\n" + "a man is a MAN but bubblesort is a dog\n"
                + "the OLD man and the sea\n" + "a PORTRAIT of the artist as a young man\n"
                + "the old man and the SEA\n" + "a portrait of the artist as a YOUNG man\n";
        assertEquals( KWIC.run( input ), output );
        try {
            KWIC.run( input1 );
            fail();
        }
        catch ( IllegalArgumentException e ) {
            assertNotNull( e );
        }
        try {
            KWIC.run( input2 );
            fail();
        }
        catch ( IllegalArgumentException e ) {
            assertNotNull( e );
        }
        try {
            KWIC.run( input3 );
            fail();
        }
        catch ( IllegalArgumentException e ) {
            assertNotNull( e );
        }
        try {
            KWIC.run( input4 );
            fail();
        }
        catch ( IllegalArgumentException e ) {
            assertNotNull( e );
        }
        try {
            KWIC.run( input5 );
            fail();
        }
        catch ( IllegalArgumentException e ) {
            assertNotNull( e );
        }
    }

}
