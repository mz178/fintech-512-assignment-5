package edu.duke.fintech512.terms;

import static org.junit.Assert.*;

import org.junit.jupiter.api.*;

class TermFreqTest {

    @Test
    void test () {
        String lion = "Output:\n" + "live - 2\n" + "mostly - 2\n" + "africa - 1\n" + "india - 1\n" + "lions - 1\n"
                + "tigers - 1\n" + "white - 1\n" + "wild - 1\n";
        assertEquals( TermFreq.run( "./test_files/lion.txt" ), lion );
        try {
            TermFreq.run( "Wrong.file" );
            fail();
        }
        catch ( IllegalArgumentException e ) {
            assertEquals( e.getMessage(), "File not Found" );
        }
    }

}
