# FINTECH 512 Assignment 5

NOTE: For each task, take note of how long it took you, in hours and minues, and write it down.

**Read [2-3 hours, 1 point]**

NOTE: Credit for reading is established by writing and submitting notes on what you read.

Skim this free chapter from Refactoring for an extended example of refactoring. Alternatively, you can watch Martin Fowler give a talk on the chapter's example here

Read the Class/Class Diagram and Interaction Diagrams sections of Holub's UML Quick Reference You'll need this as a reference for the Do part of the assignment. No notes necessary here, you'll demonstrate this through creating UML class and sequence diagrams.

Before your O'Reilly trial ends:

Take half an hour or so to skim chapters 3, 6, and 7 from Analysis Patterns: Reusable Object Models
Take half an hour or so to read as much of part 1 of Domain Driven Design: Tackling Complexity in the Heart of Software as you have time for. Be sure to read Evan's definition of Value Object, and look at how it fits into the 'building blocks'.

**Watch**

No videos this week.

**Do [4+ hours, 2 points]**

Create a GitLab repo named 'assignment5' for this week's work. Add me as a 'Reporter'

Draw UML class diagrams for Ledger's xact and account classes. Submit them as .png or .jpg files in the repo. You may use any diagramming tool you find convenient, ranging from pen and paper to Draw.io, LucidChart, OmniGraffle, Visio. I'd suggest Draw.io. Post comments on Slack. As always, you are welcome to collaborate; note who your collaborator(s) are.

Draw a sequence diagram for the sequence of events caused by calling journal_t::add_account

How does Ledger represent money, e.g. US Dollars and Swiss Francs?

Complete the coding (and process) exercises below.

**Coding [5+ hours, 2 points]**

Following the PSP, TDD, and refactoring practices discussed and practiced so far in class...

Copy the code for the TermFreq application you wrote in assignment 2 and the KWIC application you wrote in assignment 3 in to the assignment 5 repo. Copy Defect.md and any other issue templates you may have created as well.
Develop a jUnit test suite for the requirements of the TermFreq application.
Develop a jUnit test suite for the requirements of the KWIC application.
Create a Java package, edu.duke.fintech512.terms to contain all code common to both the TermFreq and KWIC applications. Refactor the applications to contain as little code as possible, and for the common code to contain as much code as possible (without duplication.)
NOTE: Feel free to approach these tasks in the order that makes sense to you; follow the usual practices of PSP and frequent git commits.

**Reflection:**

Reflect on your work as soon you are done. What went well? What didn’t? What activities found the most bugs? What types of bugs caused you the most problems? Record your thoughts, but don’t try to solve problems. What did you do? What evidence did you leave? Is all of the above recorded in one or more GitLab issues?

**To Turn In**

Send me an email with your reflections when you've completed your work.